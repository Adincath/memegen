<?php 

define("FAILURL", "editFailure.php"); 
define("SUCCESSURL", "editSuccess.php"); 

$memeName;
$fileName;
$memeHeight;
$memeWidth;

if (isset($_POST["memeName"])) {
    $memeName = $_POST["memeName"];
    $fileName = $_POST["memeFileName"];
    $memeHeight =  $_POST["memeHeight"];
    $memeWidth =  $_POST["memeWidth"];
    session_start();
    $_SESSION['memeName'] = $memeName;
    $_SESSION['fileName'] = $fileName;
    addMemeToDatabase($memeName, $fileName, $memeWidth, $memeHeight);

} else {
    header('Location: ' . FAILURL, true, $statusCode);
    die();
}

function addMemeToDatabase($name, $fileName, $width, $height){
    include "dbMeta.php";

    //Attempting connection and query
    try{
        $conn = new PDO('mysql:dbname=' . DB_DATABASE . ';host=' . DB_HOST  . ';' ,DB_USERNAME , DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  

        $params = array($name, $fileName, $width, $height);
        $sql = "UPDATE " . DB_TABLE . " SET name = :name, file_name = :file_name, width = :width, height = :height ";
        $sql .= "WHERE file_name = :file_name";

        echo $sql;

        $query = $conn->prepare($sql);

        //Adding Paremterized Variables
        $query->bindParam(':name', $name);
        $query->bindParam(':file_name', $fileName);
        $query->bindParam(':width', $width);
        $query->bindParam(':height', $height);
        $query->execute();

        $conn = NULL; 
        header('Location: ' . SUCCESSURL, true, $statusCode);
        die();
    } 
    catch(PDOException $e) {
        echo 'ERROR: ' . $e->getMessage();
    }
}


?>