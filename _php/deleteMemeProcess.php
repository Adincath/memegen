<?php 

define("FAILURL", "deleteFailure.php"); 
define("SUCCESSURL", "deleteSuccess.php"); 

$memeName;
$fileName;
$memeHeight;
$memeWidth;

if (isset($_POST["memeFileName"])) {
    $fileName = $_POST["memeFileName"];
    deleteMemeImage($fileName);
    deleteMemeFromDatabase($fileName);
    session_start();
    $_SESSION['fileName'] = $fileName;

} else {
    header('Location: ' . FAILURL, true, $statusCode);
    die();
}

function deleteMemeImage($filename){
    $memeDir = "../_imgs/thumbnails/";
    unlink($memeDir . $filename);
}

function deleteMemeFromDatabase($fileName){
    include "dbMeta.php";

    //Attempting connection and query
    try{
        $conn = new PDO('mysql:dbname=' . DB_DATABASE . ';host=' . DB_HOST  . ';' ,DB_USERNAME , DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  

        $params = array($fileName);
        $sql = "DELETE FROM " . DB_TABLE . " WHERE file_name = :file_name";

        echo $sql;

        $query = $conn->prepare($sql);

        //Adding Paremterized Variables
        $query->bindParam(':file_name', $fileName);
        $query->execute();

        $conn = NULL; 
        header('Location: ' . SUCCESSURL, true, $statusCode);
        die();
    } 
    catch(PDOException $e) {
        echo 'ERROR: ' . $e->getMessage();
    }
}


?>